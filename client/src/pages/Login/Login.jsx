import React from "react";
import { Button, Form } from "react-bootstrap";
import { login } from "../../api/api";
import "./Login.scss";
const Login = ({ setCurrentUser }) => {
  const handleSubmit = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity()) {
      try {
        const res = await login({
          username: form.username.value,
          password: form.password.value,
        });
        setCurrentUser(res);
      } catch (error) {
        setCurrentUser(null);
        alert("Your login data is incorrect. Please try again");
      }
    }
  };

  return (
    <div className="login">
      <h1>Login</h1>
      <Form autoComplete="off" onSubmit={handleSubmit}>
        <Form.Group className="mb-3 form-group" controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control required type="text" placeholder="Username" />
        </Form.Group>
        <Form.Group className="mb-3 form-group" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control required type="password" placeholder="Password" />
        </Form.Group>
        <Button type="submit">Log in</Button>
      </Form>
    </div>
  );
};

export default Login;
