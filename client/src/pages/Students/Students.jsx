import React, { useEffect, useState } from "react";
import { BiPlus } from "react-icons/bi";
import { nanoid } from "nanoid";
import moment from "moment";
import { Button, Form, Table } from "react-bootstrap";

import { StudentsTableItem } from "../../components/StudentsTableItem/StudentsTableItem";
import { StudentModal } from "../../components/StudentModal/StudentModal";
import "./Students.scss";
import {
  createStudent,
  getGroups,
  getStudents,
  updateStudent,
  deleteStudent,
} from "../../api/api";
import ErorrModal from "../../components/ErrorModal/ErorrModal";

const Students = () => {
  const [showModal, setShowModal] = useState(false);
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [error, setError] = useState("");
  const [students, setStudents] = useState([]);
  const [groups, setGroups] = useState([]);

  const handleStudentDeletion = async (id) => {
    await deleteStudent(id);
    await requestAndSetStudents();
  };

  const handleStudentCreation = async (student) => {
    const newStudent = {
      ...student,
      id: nanoid(),
      status: "inactive",
      birthday: moment(student.birthday).format("DD.MM.YYYY"),
    };
    setShowModal(false);
    try {
      await createStudent(newStudent);
      await requestAndSetStudents();
    } catch (error) {
      setError(error?.response?.data?.error || "Problems with server");
      setShowErrorModal(true);
    }
  };

  const handleStudentUpdate = async (student) => {
    try {
      await updateStudent(student);
      await requestAndSetStudents();
    } catch (error) {
      setError(error?.response?.data?.error || "Problems with server");
      setShowErrorModal(true);
    }
  };

  const requestAndSetStudents = async () => {
    try {
      const students = await getStudents();
      setStudents(students);
    } catch (error) {
      setError(error?.response?.data?.error || "Problems with server");
      setShowErrorModal(true);
    }
  };

  useEffect(() => {
    const initializeGroups = async () => {
      const newGroups = await getGroups();
      setGroups(newGroups);
    };
    try {
      requestAndSetStudents();
      initializeGroups();
    } catch (error) {
      setError(error?.response?.data?.error || "Problems with server");
      setShowErrorModal(true);
    }
  }, []);

  return (
    <div className="students">
      <div className="title-row">
        <h1 className="title">Students</h1>
        <Button onClick={() => setShowModal(true)} variant="light">
          <BiPlus size={20} />
        </Button>
      </div>
      <Table className="table" striped bordered hover>
        <thead>
          <tr>
            <th>
              <Form className="main-checkbox">
                <Form.Check
                  type="checkbox"
                  label=""
                  id="main-checkbox"
                  defaultChecked
                />
              </Form>
            </th>
            <th>Group</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Birthday</th>
            <th>Status</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          {students?.map((student) => {
            return (
              <StudentsTableItem
                deleteStudent={handleStudentDeletion}
                key={student.id}
                editStudent={handleStudentUpdate}
                {...student}
                groups={groups}
              />
            );
          })}
        </tbody>
      </Table>
      <StudentModal
        show={showModal}
        onCancel={() => setShowModal(false)}
        onApprove={handleStudentCreation}
        groups={groups}
      />
      <ErorrModal
        show={showErrorModal}
        hide={() => setShowErrorModal(false)}
        text={error}
      />
    </div>
  );
};

export default Students;
