import cn from "classnames";
import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { IoPersonCircleSharp } from "react-icons/io5";

import "./Chat.scss";
import ChatRooms from "../../components/ChatRooms/ChatRooms";
import ChatBody from "../../components/ChatBody/ChatBody";

import { getAllRooms, getAllUsers } from "../../api/api";

const Chat = ({ currentUser, socket }) => {
  const [activeRoom, setActiveRoom] = useState({});
  const [users, setUsers] = useState([]);
  const [messages, setMessages] = useState([]);
  const [sender, setSender] = useState(currentUser);
  const [message, setMessage] = useState("");

  const [rooms, setRooms] = useState([]);

  useEffect(() => {
    socket.on("message", (newMessage) => {
      setMessages([...messages, newMessage]);
    });
    socket.on("messageHistory", (messages) => {
      setMessages(messages);
    });
  }, [messages]);

  useEffect(() => {
    const setInitialData = async () => {
      const users = await getAllUsers();
      const rooms = await getAllRooms(currentUser);
      setUsers(users);
      setRooms(rooms);
    };
    setInitialData();
  }, []);

  const handleJoinRoom = (roomId) => {
    socket.emit("join", roomId, sender);
  };

  const handleSendMessage = (message) => {
    socket.emit("message", activeRoom._id, sender, message);
    setMessage("");
  };

  return (
    <div className="chat">
      <h1 className="title">Messages</h1>
      <main>
        <ChatRooms
          rooms={rooms}
          activeRoom={activeRoom}
          setActiveRoom={setActiveRoom}
          joinRoom={handleJoinRoom}
          users={users}
        />
        <ChatBody
          activeRoom={activeRoom}
          messages={messages}
          users={users}
          currentUser={currentUser}
          sendMessage={handleSendMessage}
        />
      </main>
    </div>
  );
};

export default Chat;
