import './App.scss';
import Header from './components/Header/Header';
import NavbarContainer from './components/Navbar/Navbar';
import { Navigate, Route, Routes } from 'react-router-dom';
import Students from './pages/Students/Students';
import Layout from './components/Layout/Layout';
import Login from './pages/Login/Login';
import { useEffect, useState } from 'react';
import Chat from './pages/Chat/Chat';
import io from "socket.io-client";
import { connectToAllRooms } from './api/api';

function App() {
	const [currentUser, setCurrentUser] = useState(null)
	const socket = io("http://localhost:5001");

	useEffect(() => {
		const connectToRooms = async () => {
			connectToAllRooms(socket, currentUser);
		}
		if (currentUser) {
			connectToRooms();
		}
	}, [currentUser])

	return (
		<>
			{
				currentUser ?
					<>
						<Header socket={socket} id={currentUser} />
						<NavbarContainer />
						<Routes>
							<Route path="/" element={<Layout />}>
								<Route path='students' element={<Students />} />
								<Route path='chat' element={<Chat socket={socket} currentUser={currentUser} />} />
								<Route path='' element={<Navigate to='/students' replace />} />
							</Route>
						</Routes></>
					:
					<Login setCurrentUser={setCurrentUser} />

			}

		</>
	);
}

export default App;
