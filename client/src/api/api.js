import axios from 'axios';
const URL = 'http://localhost/server/'

export const getStudents = async () => {
	try {
		const res = await axios.get(URL, {
			params: {
				students: true
			}
		})
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const getGroups = async () => {
	try {
		const res = await axios.get(URL, {
			params: {
				groups: true
			}
		})
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const createStudent = async (student) => {
	try {
		const res = await axios.post(URL, JSON.stringify(student))
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}

}

export const updateStudent = async (student) => {
	try {
		const res = await axios.put(URL, JSON.stringify(student))
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const deleteStudent = async (id) => {
	try {
		const res = await axios.delete(URL + `?id=${id}`)
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const login = async (loginData) => {
	try {
		const res = await axios.post('http://localhost:5000/login',

			loginData
		)
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const getAllUsers = async () => {
	try {
		const res = await axios.get('http://localhost:5000/')
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const getUser = async (id) => {
	try {
		const res = await axios.get(`http://localhost:5000/user?id=${id}`)
		return res.data[0]
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const getAllRooms = async (id) => {
	try {
		const res = await axios.get(`http://localhost:5000/rooms?id=${id}`,)
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}
export const getLastMessages = async (id, count) => {
	try {
		const res = await axios.get(`http://localhost:5000/rooms/last-messages?id=${id}&count=${count || 3}`)
		return res.data
	} catch (error) {
		console.error(error);
		throw error
	}
}

export const connectToAllRooms = async (socket, userId) => {
	const rooms = await getAllRooms(userId);
	for (const room of rooms) {
		socket.emit("join", room._id, userId);
	}
}