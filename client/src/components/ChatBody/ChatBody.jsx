import React from "react";
import { IoPersonCircleSharp, IoAddCircleSharp } from "react-icons/io5";
import Message from "../Message/Message";
import { Button, Form, FormGroup, InputGroup } from "react-bootstrap";
import "./ChatBody.scss";
const ChatBody = ({
  messages,
  activeRoom,
  users,
  currentUser,
  sendMessage,
}) => {
  const formatMembers = () => {
    return activeRoom?.members
      ?.map((id) => {
        return users.find((user) => user._id === id)?.username;
      })
      ?.join(" , ");
  };

  const handleMessageSend = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    const value = form.message.value;
    if (value.trim().length) {
      sendMessage(value);
      form.message.value = "";
    }
  };
  return (
    <div className="chat-body">
      <h1 className="title">Chat room {formatMembers()}</h1>
      <div className="members">
        <h2>Members</h2>
        <div className="members-list">
          {Array(activeRoom?.members?.length || 0)
            .fill(0)
            .map((el) => (
              <IoPersonCircleSharp size={25} />
            ))}
        </div>
      </div>
      <h2 className="section-title">Messages</h2>

      <div className="messages">
        {messages?.map((el, index) => {
          const align = el.from === currentUser ? "right" : "left";
          return <Message key={el._id} text={el.message} align={align} />;
        })}
      </div>
      <div className="write-message">
        <Form onSubmit={handleMessageSend}>
          <FormGroup className="input-message" controlId="message">
            <Form.Control />
            <Button type="submit">Send</Button>
          </FormGroup>
        </Form>
      </div>
    </div>
  );
};
export default ChatBody;
