import cn from "classnames";
import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";

import "./Navbar.scss";

const NavbarContainer = () => {
  return (
    <Navbar style={{ padding: "40px 0 0 40px" }} className="navbar-container">
      <Nav className={cn("flex-column")}>
        <NavLink to="/dashboard">Dashboard</NavLink>
        <NavLink to="/students">Students</NavLink>
        <NavLink to="/tasks">Tasks</NavLink>
      </Nav>
    </Navbar>
  );
};

export default NavbarContainer;
