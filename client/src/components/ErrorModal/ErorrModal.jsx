import React, { useEffect } from "react";
import { Alert, Modal } from "react-bootstrap";
import "./ErrorModal.scss";
const ErorrModal = ({ show, hide, text }) => {
  return (
    <>
      <Modal centered className="error-modal" show={show} onHide={hide}>
        <Modal.Body>
          <Alert variant={"danger"}>
            <Alert.Heading>Error</Alert.Heading>
            <p>{text}</p>
          </Alert>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default ErorrModal;
