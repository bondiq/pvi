import React, { useEffect, useState } from "react";
import { MdOutlineModeEditOutline } from "react-icons/md";
import { IoMdClose } from "react-icons/io";
import cn from "classnames";
import moment from "moment";
import { Button, Form } from "react-bootstrap";

import { WarningModal } from "../WarningModal/WarningModal";
import { StudentModal } from "../StudentModal/StudentModal";
import "./StudentsTableIten.scss";

export const StudentsTableItem = ({
  id,
  status,
  group,
  firstName,
  lastName,
  gender,
  birthday,
  deleteStudent,
  editStudent,
  groups,
}) => {
  const [checked, setChecked] = useState(status === "active");
  const [showWarningModal, setShowWarningModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);

  const handleDeletion = () => {
    setShowWarningModal(false);
    deleteStudent(id);
  };

  const handleEdition = (student) => {
    setShowEditModal(false);
    editStudent({
      ...student,
      id,
      birthday: moment(student.birthday).format("DD.MM.YYYY"),
    });
  };

  useEffect(() => {
    setChecked(status === "active");
  }, [status]);

  return (
    <>
      <tr>
        <td>
          <Form className="form-checkbox">
            <Form.Check
              type="checkbox"
              label=""
              id="checkbox"
              onChange={() => setChecked(!checked)}
              checked={checked}
            />
          </Form>
        </td>
        <td>{group}</td>
        <td>{firstName + " " + lastName}</td>
        <td>{gender?.[0]?.toUpperCase()}</td>
        <td>{moment(birthday, "YYYY-MM-DD").format("DD.MM.YYYY")}</td>
        <td>
          <span
            className={cn("status", {
              active: checked,
              inactive: !checked,
            })}
          ></span>
        </td>
        <td>
          <Button onClick={() => setShowEditModal(true)} variant="light">
            <MdOutlineModeEditOutline size={20} />
          </Button>{" "}
          <Button onClick={() => setShowWarningModal(true)} variant="light">
            <IoMdClose size={20} />
          </Button>
        </td>
      </tr>

      <WarningModal
        show={showWarningModal}
        onCancel={() => setShowWarningModal(false)}
        onApprove={handleDeletion}
        student={firstName + " " + lastName}
      />
      <StudentModal
        show={showEditModal}
        onCancel={() => setShowEditModal(false)}
        mode="editing"
        onApprove={handleEdition}
        groups={groups}
        defaultValues={{
          firstName,
          lastName,
          group,
          gender,
          birthday,
        }}
      />
    </>
  );
};
