import cn from "classnames";
import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { IoPersonCircleSharp } from "react-icons/io5";
import "./ChatRooms.scss";
const ChatRooms = ({ activeRoom, joinRoom, setActiveRoom, users, rooms }) => {
  const formatMembers = (ids) => {
    return ids
      ?.map((id) => {
        return users.find((user) => user._id === id)?.username;
      })
      ?.join(" , ");
  };

  const selectRoom = (room) => {
    joinRoom(room._id);
    setActiveRoom(room);
  };

  useEffect(() => {
    if (rooms?.length) {
      console.log(rooms[0]);
      selectRoom(rooms[0]);
    }
  }, [rooms]);
  return (
    <div className="chat-rooms">
      <div className="title">
        <h1>Chat room</h1>
        <Button variant="light">+ New chat room</Button>
      </div>
      <div className="room-list">
        {rooms
          .sort((a, b) => {
            if (a._id === activeRoom._id) return -1;
            if (b._id === activeRoom._id) return 1;
            return 0;
          })
          .map((room) => {
            return (
              <Card
                onClick={() => selectRoom(room)}
                className={cn("room", {
                  "active-room": room._id === activeRoom._id,
                })}
                key={room.id}
                body
              >
                <div>
                  <IoPersonCircleSharp size={25} />
                </div>
                <p> {formatMembers(room.members)}</p>
              </Card>
            );
          })}
      </div>
    </div>
  );
};

export default ChatRooms;
