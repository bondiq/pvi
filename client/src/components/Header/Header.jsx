import React, { useEffect, useState } from "react";
import cn from "classnames";
import { BsPersonCircle, BsBell } from "react-icons/bs";

import "./Header.scss";
import { useNavigate } from "react-router-dom";
import { getLastMessages, getUser } from "../../api/api";

const Header = ({ socket, id }) => {
  const [receivedMessage, setReceivedMessage] = useState(false);
  const [currentUser, setCurrentUser] = useState({});
  const [lastMessage, setLastMessage] = useState([]);
  const navigate = useNavigate();

  const getAndSetLasMessages = async () => {
    const messages = await getLastMessages(id, 3);
    setLastMessage(messages);
  };

  useEffect(() => {
    const getAndSetData = async () => {
      const user = await getUser(id);
      setCurrentUser(user);
      await getAndSetLasMessages();
    };

    getAndSetData();
    socket.on("message", () => {
      setReceivedMessage(true);
      getAndSetLasMessages();
    });
  }, []);

  return (
    <header className="header">
      <div className="logo">CMS</div>
      <div className="menu">
        <div className="notification">
          <div className="notification-logo">
            <div
              className={cn({ "bell-animation": receivedMessage })}
              onAnimationEnd={() => setReceivedMessage(false)}
              onClick={() => navigate("/chat")}
            >
              <BsBell fill="#fff" cursor={"pointer"} size={25} />
            </div>
            <div
              className={cn("indicator", { "show-indicator": receivedMessage })}
            ></div>
          </div>
          <div className="dropdown-content">
            {lastMessage?.map((message) => {
              return (
                <div className="dropdown-content-element">
                  <div className="other-user">
                    <BsPersonCircle className="other-user-logo" size="30px" />
                    <div className="other-user-name"></div>
                  </div>
                  <div className="message">{message.message}</div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="user">
          <BsPersonCircle fill="#fff" size="35px" />
          <div className="user-fullname">{currentUser?.username}</div>
          <div className="dropdown-content">
            <a href="#profile">Profile</a>
            <a href="#log-out">Log out</a>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
