import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

import "./StudentModal.scss";
import { getGroups } from "../../api/api";
export const StudentModal = ({
  show,
  onCancel,
  onApprove,
  mode,
  groups,
  defaultValues = {},
}) => {
  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    setValidated(true);
    if (form.checkValidity()) {
      onApprove({
        group: groups.find((group) => group.name === form.group.value)?.id,
        firstName: form.firstName.value,
        lastName: form.lastName.value,
        gender: form.gender.value,
        birthday: form.birthday.value,
      });
      setValidated(false);
    }
  };

  const handleHide = () => {
    setValidated(false);
    onCancel();
  };
  return (
    <>
      <Modal size="md" show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>
            {mode === "editing" ? "Edit" : "Add"} student
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form noValidate={true} validated={validated} onSubmit={handleSubmit}>
            <Form.Group className="mb-3 form-group" controlId="group">
              <Form.Label>Group</Form.Label>
              <Form.Select defaultValue={defaultValues.group || ""} required>
                {groups?.map((group) => (
                  <option key={group.id} value={group.name}>
                    {group.name}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="firstName">
              <Form.Label>First name</Form.Label>
              <Form.Control
                defaultValue={defaultValues.firstName || ""}
                required
                pattern="^[a-zA-Zа-яА-Я]{2,}$"
                type="text"
                placeholder="First name"
              />
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="lastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                defaultValue={defaultValues.lastName || ""}
                pattern="^[a-zA-Zа-яА-Я]{2,}$"
                required
                type="text"
                placeholder="Last name"
              />
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="gender">
              <Form.Label>Gender</Form.Label>
              <Form.Select defaultValue={defaultValues.gender || ""} required>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3 form-group" controlId="birthday">
              <Form.Label>Birthday</Form.Label>
              <Form.Control
                defaultValue={defaultValues.birthday || ""}
                required
                max="2012-12-31"
                min="1945-01-01"
                type="date"
              />
            </Form.Group>
            <Modal.Footer>
              <Button type="submit" variant="primary">
                {mode === "editing" ? "Save" : "Create"}
              </Button>
              <Button variant="secondary" onClick={handleHide}>
                Cancel
              </Button>
            </Modal.Footer>{" "}
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};
