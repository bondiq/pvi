import cn from "classnames";
import React from "react";
import { IoPersonCircleSharp, IoAddCircleSharp } from "react-icons/io5";

import "./Message.scss";
const Message = ({ align, text }) => {
  return (
    <div
      className={cn("message", {
        right: align === "right",
        left: align === "left",
      })}
    >
      <div className="small-row">
        <p>{text}</p>
        <div>
          {" "}
          <IoPersonCircleSharp display={"block"} size={25} />
        </div>
      </div>
    </div>
  );
};

export default Message;
