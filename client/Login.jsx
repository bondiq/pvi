import React from "react";
import { Button, Form } from "react-bootstrap";
import { login } from "../../api/api";
import "./Login.scss";
const Login = ({ setIslogged }) => {
  const handleSubmit = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity()) {
      try {
        const res = await login({
          username: form.username.value,
          password: form.password.value,
        });
        setIslogged(true);
      } catch (error) {
        setIslogged(false);
        alert("Your login data is incorrect. Please try again");
      }
    }
  };

  return (
    <div className="login">
      <h1>Login</h1>
      <Form autocomplete="off" onSubmit={handleSubmit}>
        <Form.Group className="mb-3 form-group" controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control
            autocomplete="off"
            required
            type="text"
            placeholder="Username"
          />
        </Form.Group>
        <Form.Group className="mb-3 form-group" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            autocomplete="off"
            required
            type="password"
            placeholder="Password"
          />
        </Form.Group>
        <Button type="submit">Log in</Button>
      </Form>
    </div>
  );
};

export default Login;
