import express from 'express';
import User from './user-schema.js'
import Message from './message-schema.js'
import ChatRoom from './chat-room-schema.js'
import { Server } from "socket.io";
import mongoose from 'mongoose';

const PORT = 5000
const DB = 'mongodb+srv://user:user@pvi.lmvwgst.mongodb.net/pvi?retryWrites=true&w=majority'
const app = express();

app.use(express.json())

app.post('/', async (req, res) => {
	const { username, password } = req.body;
	const user = await User.create({ username, password })
	res.status(200).json(user._id)
})

app.get('/', async (req, res) => {
	const users = await User.find({})
	res.status(200).json(users)

})

app.get('/user', async (req, res) => {
	const id = req.query.id;
	const user = await User.find({ _id: id })
	res.status(200).json(user)

})

app.get('/rooms', async (req, res) => {
	const id = req.query.id;
	const rooms = await ChatRoom.find({ members: id })
	res.status(200).json(rooms)

})

app.get('/rooms/last-messages', async (req, res) => {
	const userId = req.query.id;
	const messagesCount = req.query.count ?? 3
	const rooms = await ChatRoom.find({ members: userId });
	const messages = await Message.find({ room: { $in: rooms.map(room => room._id) } })
		.sort({ createdAt: -1 })
		.limit(messagesCount)
	res.status(200).json(messages)

})

app.post('/login', async (req, res) => {
	const { username, password } = req.body;
	const user = await User.findOne({ username, password })
	if (user) {
		res.status(200).json(user.id)
	} else {
		res.status(400).json("There are not such user")
	}
})

const io = new Server(5001, {
})

io.on('connection', socket => {
	console.log('A user connected');
	socket.on('join', async (roomId, userId,) => {
		let room = roomId
		const roomExists = await ChatRoom.exists({ _id: roomId });
		if (!roomExists) {
			const newRoom = await ChatRoom.create({ members: [userId] })
			room = newRoom.id
		}

		socket.join(room);
		socket.to(room).emit('userJoined', userId);

		const messages = (await Message.find({ room })
			.sort('-createdAt')
			.exec())?.reverse();
		socket.emit('messageHistory', messages);
	});

	socket.on('message', async (room, from, message) => {
		const newMessage = await Message.create({ room, from, message });
		if (newMessage) {
			io.to(room).emit('message', newMessage);
		} else {
			console.log('error');
		}
	});

	socket.on('disconnect', () => {
		console.log('A user disconnected');
	});
});



const appBootstrap = async () => {
	try {
		await mongoose.connect(DB);
		app.listen(PORT, () => {
			console.log('server works');
		});
	} catch (e) {
		console.log(e);
	}
}

appBootstrap();