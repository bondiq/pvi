import mongoose from "mongoose";

const ChatRoom = new mongoose.Schema({
	members: [String],
	createdAt: { type: Date, default: Date.now }
})

export default mongoose.model('ChatRoom', ChatRoom)