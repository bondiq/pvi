<?php

$servername = "localhost";
$username = "root";
$password = "xxxY1337Yxxx";
$dbname = "pvi";

try {
	$pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	http_response_code(500);
	die(json_encode(array('error' => "Sorry, we have problems with connecting to database, so some actions will be non-working")));
}

?>