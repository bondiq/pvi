<?php

require_once 'config.php';
error_reporting(E_ERROR | E_PARSE);

function verify($data, $checkId)
{
	$firstName = $data['firstName'];
	$lastName = $data['lastName'];
	$group = $data['group'];
	$gender = strtolower($data['gender']);
	$birthday = $data['birthday'];
	$id = $data['id'];

	global $pdo;


	$nameRegex = "/^[a-zA-Zа-яА-Я]{2,}$/";
	$genders = array('male', 'female');
	$response = [];

	if (!preg_match($nameRegex, $firstName) || !preg_match($nameRegex, $lastName)) {
		$response["error"] = "Name is incorrect!";
		return $response;
	}

	$groupsList = $pdo->prepare("SELECT id FROM students_groups WHERE id = ?");
	$groupsList->execute([$group]);
	if (!$groupsList->fetchColumn()) {
		$response["error"] .= "\nGroup is incorrect!";
		return $response;
	}

	if (!in_array($gender, $genders)) {
		$response["error"] .= "\nGender is incorrect!";
		return $response;
	}

	$minYear = 1950;
	$maxYear = 2010;
	$currentYear = date('Y', strtotime($birthday));

	if ($currentYear > $maxYear || $currentYear < $minYear) {
		$response["error"] .= "\nDate birthday must be between " . $minYear . " and " . $maxYear;
		return $response;
	}

	if ($checkId) {
		$ids = $pdo->prepare("SELECT id FROM students WHERE id = ?");
		$ids->execute([$id]);
		if (!$ids->fetchColumn()) {
			$response["error"] .= "\nThere is not student with this id";
			return $response;
		}
	}

	$response["firstName"] = $firstName;
	$response["lastName"] = $lastName;
	$response["group"] = $group;
	$response["birthday"] = date('Y-m-d', strtotime($birthday));
	$response["gender"] = $gender;
	$response["id"] = $id;

	return $response;
}

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['students'])) {
	$stmt = $pdo->prepare('SELECT students.*, students_groups.name AS `group` 
	FROM students 
	INNER JOIN students_groups 
	ON students.group_id = students_groups.id');
	$stmt->execute();
	$students = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach ($students as &$student) {
		$student['firstName'] = $student['firstname'];
		unset($student['firstname']);

		$student['lastName'] = $student['lastname'];
		unset($student['lastname']);
	}
	header('Content-Type: application/json');
	echo json_encode($students);
}

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['groups'])) {
	$stmt = $pdo->prepare('SELECT * FROM students_groups');
	$stmt->execute();
	$groups = $stmt->fetchAll(PDO::FETCH_ASSOC);
	header('Content-Type: application/json');
	echo json_encode($groups);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$data = json_decode(file_get_contents('php://input'), true);
	if ($data['login']) {

		$stmt = $pdo->prepare('SELECT id FROM login WHERE username = :username AND password = :password');
		$stmt->execute([
			'username' => $data['data']['username'],
			'password' => $data['data']['password'],
		]);
		$result = $stmt->fetch();
		if ($result) {
			echo json_encode($result);

		} else {
			http_response_code(400);
			echo json_encode(["error" => "not in database"]);
		}
		exit();
	}

	// http_response_code(400);

}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$data = json_decode(file_get_contents('php://input'), true);
	$result = verify($data, false);
	if (!$result['error']) {
		$stmt = $pdo->prepare('INSERT INTO students (firstname, lastname, birthday, group_id, gender) VALUES (?, ?, ?, ?, ?)');
		$stmt->execute([
			$result['firstName'],
			$result['lastName'],
			$result['birthday'],
			$result['group'],
			$result['gender'],
		]);

		$idOfCreatedStudent = $pdo->lastInsertId();
		echo json_encode(['id' => $idOfCreatedStudent]);
		exit();
	}
	http_response_code(400);
	echo json_encode($result);
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
	$data = json_decode(file_get_contents('php://input'), true);
	$result = verify($data, true);

	if (!$result['error']) {
		$stmt = $pdo->prepare('UPDATE students SET firstname = ?, lastname = ?, birthday = ?, group_id = ?, gender = ? WHERE id = ?');
		echo json_encode($result);

		$stmt->execute([
			$result['firstName'],
			$result['lastName'],
			$result['birthday'],
			$result['group'],
			$result['gender'],
			$result['id'],

		]);
		echo json_encode(['id' => $result['id']]);
		exit();
	}

	http_response_code(400);
	echo json_encode($result);
}

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
	$id = $_GET['id'];
	$stmt = $pdo->prepare('DELETE FROM students WHERE id = ?');
	$success = $stmt->execute([$id]);
	if ($success) {
		header('Content-Type: application/json');
		echo json_encode(['id' => $id]);
		exit();
	}
	http_response_code(400);
	echo json_encode(['error' => "Error with deletion"]);

}

?>